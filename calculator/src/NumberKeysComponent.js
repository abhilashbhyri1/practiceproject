import React from 'react';
import { Button } from 'react-bootstrap';
class NumberKeysComponent extends React.Component {
  constructor() {
    super()
    this.state = {
      quantity: 0,
      counter: 0
    }

  }
  render(a){ 
 const arr =[];
    const buttonNames =[,'(','CE',')','C',1,2,3,4,5,6,7,8,9,0,'+','-','/','*','='];
    let temp =0;
    return (
        <div>
          <div class="container">
  <div >
   {buttonNames.map((d)=>{
      return <Button onClick={e => this.props.onClick(e.target.value)} value={d} style={{width:'155px', height:'63px',fontWeight:'bold',padding:'3px',margin:'3px'}}  >{d}</Button> 
        })}
    
  </div>
  </div>
      
        </div>
    )
  }
    
}

export default NumberKeysComponent;
