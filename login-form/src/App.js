import logo from './logo.svg';
import './App.css';
import LoginForm from './LoginForm';
import Header from './Header';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import TableData from './TableData';
import Home from './Home';
import Contact from './Contact';
function App() {
  
  return (
    <div className="App">
      {/* <Header/>
    <LoginForm/> */}
    <Router>
              
              <div >
                        
                      
                          
                          <Switch>
                          <Route path="/" component= {Header}></Route>


                         </Switch>
                         <Route path="/home" component= {Home}></Route>

                         <Route path="/table-data" component= {TableData}></Route>
                         <Route path="/contact" component= {Contact}></Route>
                          
                       
              </div>
      </Router>
  
    </div>
  );
}

export default App;
