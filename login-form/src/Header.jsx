
import React, { PureComponent } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SearchIcon from '@material-ui/icons/Search';
import { fade,withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
// import {useHistory} from 'react-router-dom';
import './styles.css'

const style = {
    flexGrow: 1,
    color : 'black',
    iconContainerStyle : {marginRight: 100}
}


const styles = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(1),
          width: 'auto',
        },
      },
      searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color:'black'
      },
      inputRoot: {
        color: 'black',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 0),

        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          width: '12ch',
          '&:focus': {
            width: '20ch',
          },
        },
      },
  });

  

class Header extends PureComponent {
    
    constructor(props){
        super(props)
        // this.addUser = this.addUser.bind(this)
        this.state = {
            value : 0,
            
        }
        
    }
    
    handleChange = (event, value) => {
      const { history } = this.props;
      console.log(value)
      switch (value) {
        case 0:
        this.props.history.push("/home")
          
          break;
        case 1:
        this.props.history.push("/table-data")
          break;
        case 2:
          this.props.history.push("/contact")
          break;
        // case 3:
        //   this.props.history.push("/add4")
        //   break;
      
      }
      // if(value===1){
      //   this.props.history.push("/add")
      // }
        this.setState({ value });
      };

    // redirectToHome = () => {
    //     const { history } = this.props;
    //     if(history) history.push('/add');
    //    }
     
     
    // addUser(){
    //     this.props.history.push('/add');
    // }

    render() {

        const { classes } = this.props;
        
        return (
            <div>
                <div className="container-fluid" style={boot}>
                <AppBar position="static" style={header}>
                        <Toolbar>
                            <Typography variant="h6" style={style}>
                                React Header
                            </Typography>
                                
                            <Tabs
                            style = {tabs}
                            value={this.state.value}
                            onChange={this.handleChange}
                            indicatorColor="white"
                            textColor="black"
                            >
                            
                            <Tab label="Home" className = "tab"  />
                            
                            <Tab label="Employees Data" className = "tab" style={{minHeight:'80px'}} />
                            
                            {/* <Tab label="Sell Digital" className = "tab"/> */}
                            <Tab label="Contact" className = "tab"/>
                            </Tabs>

                            
                            {/* <SearchIcon style={search} className={classes.searchIcon}/> */}
                            <div className={classes.search}>
                                    <div className={classes.searchIcon}>
                                    <SearchIcon />
                                    </div>
                                    <InputBase
                                    placeholder="Search…"
                                    classes={{
                                        root: classes.inputRoot,
                                        input: classes.inputInput,
                                    }}
                                    inputProps={{ 'aria-label': 'search' }}
                                    />
                            </div>
                            
                    
                        </Toolbar>
                        


                    </AppBar>
                                    {console.log("PROPS",this.props.location.pathname)}
                                    {this.props.location.pathname === '/home' ?
                    <div>sfsdgsdgs</div> : null
                                    
                                  }
                    </div>
            </div>
        );
    }
}

const header = {
    backgroundColor: 'white',
    height : '80px',
}

const tabs = {
    marginRight: 300,
    color : 'black',
   
}

// const search = {
//     color : 'black',
    
// }

const boot={
    paddingLeft:'0px',
    paddingRight:'0px'
}
export default withStyles(styles, { withTheme: true })(Header);


//onClick={() => this.addUser()}