import React from 'react'
import MaterialTable from 'material-table';
const axios = require('axios');
 export default function Home() {
  const { useState,useEffect } = React;
  const [data, setData] = useState([]
  //   [
  //   { name: 'Mehmet', surname: 'Baran', birthYear: 1987, birthCity: 63 },
  //   { name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34 },
  // ]
  );
  const [columns, setColumns] = useState([
    { title: 'ID', field: 'id' },
    { title: 'Title', field: 'title', initialEditValue: 'initial edit value' },
    // { title: 'Author', field: 'author' },
   
  ]);

  useEffect(()=>{
    // alert("hi");
    axios.get('https://my-json-server.typicode.com/typicode/demo/posts',{
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept':'*',
        'Access-Control-Allow-Origin': 'http://localhost:3000'
      }
     })
    .then(function (response) {
      console.log("FFFFFFF",response)
      console.log(response.data);
      setData(response.data)
})
    .catch(function (error) {
      console.log(error);
    })
  },[])


 

  return (
    <MaterialTable
      title="List of the Posts Using Axios"
      columns={columns}
      data={data}
    
    />
  )
}
