import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
// import { useHistory } from "react-router-dom";np
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  '& > *': {
    margin: theme.spacing(1),
    width: '25ch',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function LoginForm() {
  const classes = useStyles();
  // let history = useHistory();
  const [formValues,setFormValues] = useState({
    username:'',
    password:'',
    validationMsg :'Please fill Username/Password',
    displayMsg:false
  })

  console.log("DDDDDd",formValues)
  let loginValidation =()=>{
    if(formValues.username == '' || formValues.password == ''){
    setFormValues({...formValues,displayMsg:true})
    }
    else{
    // history.push('/home')
    }
    // alert("Hi");
  }
  return (
    <div className={classes.root}>
    <h1>Login Form</h1>

      <Grid container spacing={3} style={{marginTop:"10px"}}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
          <form className={classes.root} noValidate autoComplete="off">
      <TextField id="outlined-basic" label="User Name" onChange={(e)=>{setFormValues({...formValues,displayMsg:false,username:e.target.value})}} variant="outlined" /> <br/><br/>
      <TextField id="outlined-basic" label="Password" type="password" onChange={(e)=>{setFormValues({...formValues,displayMsg:false,password:e.target.value})}}  variant="outlined" /> <br/><br/>
     
      {formValues.displayMsg ?
    <Alert severity="error">{formValues.validationMsg}</Alert>
   :
   null }
      <Button variant="contained" color="primary" onClick={loginValidation}>
          Login
        </Button>
        <Button variant="contained" style={{margin:"0px 1%"}}color="primary">
          Logout
        </Button>
    </form>
  
          </Paper>
        </Grid>
        </Grid>
    </div>
  );
}
