class Employee {  
    public id: number;  
    protected Ename: string;  
    constructor(code: number, name: string){  
    this.id = code;  
    this.Ename = name;  
    }  
    public display() {  
    return (`My Employee ID is : ${this.id}, my name: ${this.Ename}.`);  
    }  
    } 
    
class EmployeeAddress extends Employee{
    private address:string;
    constructor(code: number, name: string, department: string) {  
        super(code, name);  
        this.address = department;  
    }  
    public EmployeeDetails() {  
        return (`My Employee ID is  ${this.id} \nmy name is ${this.Ename} and \nI am from  ${this.address}.`);  
    }   

}
      
    let employee: EmployeeAddress = new EmployeeAddress(40010800, "Abhilash","Telangana");  
    console.log(employee.EmployeeDetails()); 