var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Employee = /** @class */ (function () {
    function Employee(code, name) {
        this.id = code;
        this.Ename = name;
    }
    Employee.prototype.display = function () {
        return ("My Employee ID is : " + this.id + ", my name: " + this.Ename + ".");
    };
    return Employee;
}());
var EmployeeAddress = /** @class */ (function (_super) {
    __extends(EmployeeAddress, _super);
    function EmployeeAddress(code, name, department) {
        var _this = _super.call(this, code, name) || this;
        _this.address = department;
        return _this;
    }
    EmployeeAddress.prototype.EmployeeDetails = function () {
        return ("My Employee ID is  " + this.id + " \nmy name is " + this.Ename + " and \nI am from  " + this.address + ".");
    };
    return EmployeeAddress;
}(Employee));
var employee = new EmployeeAddress(40010800, "Abhilash", "Telangana");
console.log(employee.EmployeeDetails());
