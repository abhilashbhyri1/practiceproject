

interface KeyPair {
    key: number;
    value: string;
}

interface Details extends KeyPair {
    name: string;
}

let kv1: Details = { key:1, value:"Abhi" ,name:"Bhyri"};
// interface int1 implements Parent,Child {
//     Fun3():void {
        console.log(kv1);
     
//         }
// }
