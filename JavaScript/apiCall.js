// api url
const api_url =
	"https://api.github.com/users";

// Defining async function
async function getapi(url) {
	
	// Storing response
	const response = await fetch(url);
	console.log("RES",response)
	// Storing data in form of JSON
	var data = await response.json();
    // [{
    //     'name':'fggcgfcgf',
    //     'office': 'sgsg',
    //     'position':'nnnn',
    //     'salary': 10000
    // }]
	console.log(data);
	if (response) {
		hideloader();
	}
	show(data);
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
	document.getElementById('loading').style.display = 'none';
}
// Function to define innerHTML for HTML table
function show(data) {
	let tab =
		`<tr>
		<th>ID</th>
        <th>Image</th>
		<th>Name</th>
		<th>User Type</th>
        <th>Node ID</th>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
	<td>${r.id? r.id : '-'} </td>
	<td><img src="${r.avatar_url}" alt="${r.site_admin}" width="50" height="50"> </td>
	<td>${r.login? r.login : '-'} </td>
	<td>${r.site_admin == false ? 'User' : 'Admin'}</td>
	<td>${r.node_id? r.node_id : '-'} </td>
			
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("employees").innerHTML = tab;
}
